/* This program is free software: you can redistribute it and/or modify
 *
 * Copyright (C) 2013-2015 Stefan Canzar, Sandro Andreotti
 *
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <Structures.h>
#include <cstring>

namespace Parameters {

/** @defgroup Constants GlobalConstants
 *  @{
 */

enum ReadType {SINGLE_END, PAIRED_END /*, MIXED_END*/};
enum FakeMode
{
    FAKE_NONE,
    FAKE_SINGLE_END,
    FAKE_PAIRED_END,
    FAKE_PAIRED_END_TRUNC
};

extern FakeMode fake_mode;

extern ReadType mode;

///the read lenght in the experimental setup
extern UInt read_length_global;

///the fragment length for paired reads in the experimental setup
extern UInt insert_size_global;

///the gap length for paired reads in the experimental setup
extern double s_dev;

///the alpha value used for probability estimations (the RPKM value assumed for a transcript)
extern float alpha_global;

///the number of reads in million in the experimental setup
extern double M_global;

///whether we perform de novo Inference
extern bool de_novo_inference;

extern bool weights;

extern float lower_percentile;
extern float upper_percentile;
extern float theta;

extern bool quad_error;
extern bool pw_lin;
extern float epsilon;

extern double pseudoCount;
extern int normCutoff;

extern UInt endSpace;

extern UInt max_nof_exskip;

extern bool penaltyUncov;

extern bool useKnownTrans;

extern bool lengthNormRegularized;

extern bool cov_expl_by_opt;

extern bool greedy_expl_uncov;

extern int max_nof_relaxed_edges;

extern bool explainNewCover;

extern UInt unexpl_cov_bound;

extern int unexpl_cov_sum_bound;

extern double cg_regWeight;

extern double scale_lambda_cg;

extern bool verbose;

/**
 *  @}
 */

}

#endif // PARAMETERS_H
