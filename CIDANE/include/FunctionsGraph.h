/* This program is free software: you can redistribute it and/or modify
 *
 * Copyright (C) 2013-2015 Stefan Canzar, Sandro Andreotti
 *
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FUNCTIONSGRAPH_H
#define FUNCTIONSGRAPH_H

#include <seqan/find.h>

/** @defgroup Graph Functions extending seqan existing functions on Graphs *
 *  @{
 */
//////////////////////////////////////////////////////////////////////////////
// Enumerate all acyclic s-t paths
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////

/*
//for enumeratePaths (not exon aware)
template<typename TSpec, typename TVertexDescriptor, typename TPathMap, typename TAllPathsMap, typename TEdgeCargoMap>
void
_enumVisit(seqan::Graph<TSpec> const& g,
           TVertexDescriptor const u,
           TVertexDescriptor const t,
           TEdgeCargoMap const &edgeCargo,
           UInt allowedUncovered,
           UInt allowedSkip,
           UInt maxNumPaths,
           TPathMap &path,
           TAllPathsMap &all_paths
           )
{
    using namespace seqan;

    typedef typename Iterator<Graph<TSpec>, OutEdgeIterator>::Type TOutEdgeIterator;
    typedef typename Value<TEdgeCargoMap>::Type TEdgeCargo;
    appendValue(path, u);
    TOutEdgeIterator itad(g,u);
    for(;!atEnd(itad);goNext(itad))
    {
        if (seqan::length(all_paths) >= maxNumPaths)
        {
          return;
        }

        const TEdgeCargo &cargo = property(edgeCargo, *itad);

        TVertexDescriptor v = targetVertex(itad);
        if (allowedUncovered != UNDEF_UINT && cargo._isCovered == -1)
          continue;

        if (allowedUncovered == 0 && cargo._isCovered != 1)
          continue;

        if (allowedSkip != UNDEF_UINT && cargo._edgeLen > allowedSkip)
          continue;

        if (std::find(begin(path), end(path), v) == end(path)) // prevent cycles
        {
            if(v == t)
            {
                appendValue(all_paths, path);
                appendValue(back(all_paths), t);
                continue;
            }

            if (allowedUncovered == UNDEF_UINT || cargo._isCovered == 1)
              _enumVisit(g, v, t, edgeCargo, allowedUncovered, allowedSkip, maxNumPaths, path, all_paths);
            else
            {
              _enumVisit(g, v, t, edgeCargo, allowedUncovered-1, allowedSkip, maxNumPaths, path, all_paths);
            }
        }
    }
    resize(path, length(path)-1);
}
*/

//holding the const (not changing in recursive calls) parameters for _enumVisit call
struct ParamHolder
{
    ParamHolder() :
        allowedSkip(UNDEF_UINT),
        maxNumPaths(UNDEF_UINT),
        exonAware(false),
        useBounds(false){}

    UInt allowedSkip;
    UInt maxNumPaths;
    UInt lb;
    UInt ub;
    UInt lbNum;
    UInt ubNum;
    bool exonAware;
    bool useBounds;
};

//One for All
template <typename TSpec, typename TVertexDescriptor, typename TPathMap, typename TAllPathsMap, typename TEdgeCargoMap>
void
_enumVisit(seqan::Graph<TSpec> const & g,
           TVertexDescriptor const u,
           TVertexDescriptor const t,
           TEdgeCargoMap const & edgeCargo,
           UInt allowedUncovered,
           ParamHolder const & params,
           seqan::String<std::vector<TVertexDescriptor> > const & validExit,
           seqan::String<bool> const & validBlockEntry,
           TVertexDescriptor blockStartV,
           UInt prefixLength,
           TPathMap & path,
           TAllPathsMap & allPaths
           )
{
    using namespace seqan;

    typedef typename Iterator<Graph<TSpec>, OutEdgeIterator>::Type TOutEdgeIterator;
    typedef typename Value<TEdgeCargoMap>::Type TEdgeCargo;

    bool isValidExit = true;
    if (params.exonAware)
    {
        //std::cerr << "BS: " << blockStartV << std::endl;
        const std::vector<TVertexDescriptor> & valid = property(validExit, blockStartV);
        isValidExit = (valid.empty() || std::find(valid.begin(), valid.end(), u) != valid.end());
    }
    UInt ew = 0;
    UInt prefNum = length(path);

    if (prefNum + 2 > params.ubNum) // +1 for u and +1 for t
    {
        return;
    }

    //std::cerr << "Visit " << u <<  "  valid: " << isValidExit << "  enter: " << blockStartV << std::endl;
    //std::cerr << " visit: " << u << std::endl;

    appendValue(path, u);
    TOutEdgeIterator itad(g, u);
    for (; !atEnd(itad); goNext(itad))
    {
        if (seqan::length(allPaths) >= params.maxNumPaths)
        {
            return;
        }

        const TEdgeCargo & cargo = property(edgeCargo, *itad);

        if (params.useBounds)
        {
            ew = cargo._edgeWeight;

            if (ew + prefixLength > params.ub)
                continue;
        }


        TVertexDescriptor v = targetVertex(itad);

        if (v > t) // since we are on a dag this is feasible
            continue;

        if (allowedUncovered != UNDEF_UINT && cargo._isCovered == -1)
            continue;

        if (allowedUncovered == 0 && cargo._isCovered != 1)
            continue;

        if (params.allowedSkip != UNDEF_UINT && cargo._edgeLen > params.allowedSkip && (allowedUncovered == UNDEF_UINT || cargo._isCovered != 1))
            continue;

        TVertexDescriptor newBlockEnter = blockStartV;


        if (params.exonAware)
        {
            const bool newBlockEdge = cargo._intronEdge;

            if (newBlockEdge)
            {
                if (!isValidExit /* || (!empty(validBlockEntry) && !property(validBlockEntry,v))*/)
                {
                    //invalid combination of entry and exit of subexon chain
                    continue;
                }
                newBlockEnter = v;
            }
            else if (v == t && !empty(validBlockEntry) && !property(validBlockEntry, blockStartV))
            {
                //invalid entry for exon chain to right end of getExonPath
                continue;
            }
        }

        //if (std::find(begin(path), end(path), v) == end(path)) // prevent cycles  (we are on a dag. )
        //{

        if (v == t)
        {
            if ((!params.useBounds || ew + prefixLength >= params.lb) && prefNum + 1 >= params.lbNum)
            {
                appendValue(allPaths, path);
                appendValue(back(allPaths), t);
            }
            continue;
        }
        if (allowedUncovered == UNDEF_UINT || cargo._isCovered == 1)
        {
            _enumVisit(g, v, t, edgeCargo, allowedUncovered,
                       params, validExit, validBlockEntry,
                       newBlockEnter, prefixLength + ew, path, allPaths);
        }
        else
        {
            _enumVisit(g, v, t, edgeCargo, allowedUncovered - 1,
                       params, validExit, validBlockEntry,
                       newBlockEnter, prefixLength + ew, path, allPaths);
        }
        //}
    }
    resize(path, length(path) - 1);
}

/*
//for detectFeasiblePath
template<typename TSpec, typename TVertexDescriptor, typename TPathMap, typename TWeight, typename TEdgeCargoMap>
bool
_enumVisit(seqan::Graph<TSpec> const& g,
           TVertexDescriptor const u,
           TVertexDescriptor const t,
           TEdgeCargoMap const &edgeCargo,
           UInt allowedUncovered,
           UInt allowedSkip,
           TWeight prefixLength,
           TWeight lb,
           TWeight ub,
           TPathMap &path
           )
{
    using namespace seqan;

    typedef typename Iterator<Graph<TSpec>, OutEdgeIterator>::Type TOutEdgeIterator;
    typedef typename Value<TEdgeCargoMap>::Type TEdgeCargo;

    appendValue(path, u);

    //std::cerr << path << " " << prefixLength << std::endl;
    TOutEdgeIterator itad(g,u);
    for(;!atEnd(itad);goNext(itad))
    {
        const TEdgeCargo &cargo = property(edgeCargo, *itad);

        TVertexDescriptor v = targetVertex(itad);

        if (allowedUncovered != UNDEF_UINT && cargo._isCovered == -1)
          continue;

        if (allowedUncovered == 0 && cargo._isCovered != 1)
          continue;

        if (allowedSkip != UNDEF_UINT && cargo._edgeLen > allowedSkip)
          continue;


        TWeight ew = cargo._edgeWeight;

        if (ew + prefixLength > ub)
          continue;

        if (std::find(begin(path), end(path), v) == end(path)) // prevent cycles
        {
            if(v == t)
            {
              if (ew + prefixLength >= lb)
              {
                  appendValue(path, t);
                  return true;
              }
            }
            else if (v < t)
            {
              if (allowedUncovered == UNDEF_UINT || cargo._isCovered == 1)
              {
                if (_enumVisit(g, v, t, edgeCargo, allowedUncovered, allowedSkip, ew + prefixLength, lb, ub, path))
                  return true;
              }
              else if (_enumVisit(g, v, t, edgeCargo, allowedUncovered -1, allowedSkip, ew + prefixLength, lb, ub, path))
                return true;
            }
        }
    }
    resize(path, length(path)-1);
    return false;
}
*/

/*
template<typename TSpec, typename TVertexDescriptor, typename TWeight, typename TEdgeCargoMap>
void
detectFeasiblePath(seqan::Graph<TSpec> const& g,
                      TVertexDescriptor s,
                      TVertexDescriptor t,
                      TEdgeCargoMap const &edgeCargo,
                      TWeight lb,
                      TWeight ub,
                      UInt maxSkip,
                      seqan::String<TVertexDescriptor> & path)
{
    // Initialization
  ParamHolder params;
  params.allowedSkip = maxSkip;
  params.lb = lb;
  params.ub = ub;
  params.useBounds = true;
  params.maxNumPaths = 1;

  typedef seqan::String<std::vector<TVertexDescriptor> > TValidExitMap;
  seqan::StringSet<seqan::String<TVertexDescriptor> > allPaths;
  _enumVisit(g, s, t, edgeCargo, UNDEF_UINT, params, TValidExitMap(), s, 0, path, allPaths); //ValidExitMap is dummy

  clear(path);
  if (!empty(allPaths))
    path = allPaths[0];

  //_enumVisit(g, s, t, edgeCargo, UNDEF_UINT, maxSkip, 0u, lb, ub, path);
}
*/


template <typename TSpec, typename TVertexDescriptor, typename TWeight, typename TEdgeCargoMap>
void
detectFeasiblePath(seqan::Graph<TSpec> const & g,
                   TVertexDescriptor s,
                   TVertexDescriptor t,
                   const TEdgeCargoMap & edgeCargo,
                   UInt maxUncovered,
                   std::pair<TWeight, TWeight> const & minMaxEdgeLen,
                   std::pair<UInt, UInt> const & minMaxPathLen,
                   UInt maxSkip,
                   seqan::StringSet<seqan::String<TVertexDescriptor> > & allPaths,
                   UInt maxNumPaths)
{
    // Initialization
    ParamHolder params;
    params.allowedSkip = maxSkip;
    params.lb = minMaxEdgeLen.first;
    params.ub = minMaxEdgeLen.second;
    params.lbNum = minMaxPathLen.first;
    params.ubNum = minMaxPathLen.second;
    params.useBounds = true;
    params.maxNumPaths = maxNumPaths;

    typedef seqan::String<std::vector<TVertexDescriptor> > TValidExitMap;
    typedef seqan::String<bool> TValidBlockEntry;
    seqan::String<TVertexDescriptor> path;
    _enumVisit(g, s, t, edgeCargo, maxUncovered, params, TValidExitMap(), TValidBlockEntry(), s, 0, path, allPaths); //ValidExitMap is dummy
}

template <typename TSpec, typename TVertexDescriptor, typename TWeight, typename TEdgeCargoMap>
void
detectFeasiblePath(seqan::Graph<TSpec> const & g,
                   TVertexDescriptor s,
                   TVertexDescriptor t,
                   const TEdgeCargoMap & edgeCargo,
                   UInt maxUncovered,
                   std::pair<TWeight, TWeight> const & minMaxEdgeLen,
                   std::pair<UInt, UInt> const & minMaxPathLen,
                   UInt maxSkip,
                   seqan::String<TVertexDescriptor> & path)
{
    // Initialization
    ParamHolder params;
    params.allowedSkip = maxSkip;
    params.lb = minMaxEdgeLen.first;
    params.ub = minMaxEdgeLen.second;
    params.lbNum = minMaxPathLen.first;
    params.ubNum = minMaxPathLen.second;
    params.useBounds = true;
    params.maxNumPaths = 1;

    typedef seqan::String<std::vector<TVertexDescriptor> > TValidExitMap;
    typedef seqan::String<bool> TValidBlockEntry;
    seqan::StringSet<seqan::String<TVertexDescriptor> > allPaths;

    _enumVisit(g, s, t, edgeCargo, maxUncovered, params, TValidExitMap(), TValidBlockEntry(), s, 0, path, allPaths); //ValidExitMap is dummy
    clear(path);
    if (!empty(allPaths))
        path = allPaths[0];
}

template <typename TSpec, typename TVertexDescriptor, typename TWeight, typename TEdgeCargoMap>
void
detectFeasiblePath(seqan::Graph<TSpec> const & g,
                   TVertexDescriptor s,
                   TVertexDescriptor t,
                   TVertexDescriptor blockStart,
                   const TEdgeCargoMap & edgeCargo,
                   seqan::String<std::vector<TVertexDescriptor> > const & validBlockExits,
                   seqan::String<bool> const & validBlockEntry,
                   UInt maxUncovered,
                   std::pair<TWeight, TWeight> const & minMaxEdgeLen,
                   std::pair<UInt, UInt> const & minMaxPathLen,
                   UInt maxSkip,
                   seqan::StringSet<seqan::String<TVertexDescriptor> > & allPaths,
                   UInt maxNumPaths)
{
    // Initialization
    ParamHolder params;
    params.allowedSkip = maxSkip;
    params.lb = minMaxEdgeLen.first;
    params.ub = minMaxEdgeLen.second;
    params.lbNum = minMaxPathLen.first;
    params.ubNum = minMaxPathLen.second;
    params.useBounds = true;
    params.maxNumPaths = maxNumPaths;
    params.exonAware = true;

    seqan::String<TVertexDescriptor> path;
    _enumVisit(g, s, t, edgeCargo, maxUncovered, params, validBlockExits, validBlockEntry, blockStart, 0, path, allPaths);
}

template <typename TSpec, typename TVertexDescriptor, typename TWeight, typename TEdgeCargoMap>
void
detectFeasiblePath(seqan::Graph<TSpec> const & g,
                   TVertexDescriptor s,
                   TVertexDescriptor t,
                   TVertexDescriptor blockStart,
                   const TEdgeCargoMap & edgeCargo,
                   seqan::String<std::vector<TVertexDescriptor> > const & validBlockExits,
                   seqan::String<bool> const & validBlockEntry,
                   UInt maxUncovered,
                   std::pair<TWeight, TWeight> const & minMaxEdgeLen,
                   std::pair<UInt, UInt> const & minMaxPathLen,
                   UInt maxSkip,
                   seqan::String<TVertexDescriptor> & path)
{
    // Initialization
    ParamHolder params;
    params.allowedSkip = maxSkip;
    params.lb = minMaxEdgeLen.first;
    params.ub = minMaxEdgeLen.second;
    params.lbNum = minMaxPathLen.first;
    params.ubNum = minMaxPathLen.second;
    params.useBounds = true;
    params.maxNumPaths = 1;
    params.exonAware = true;

    seqan::StringSet<seqan::String<TVertexDescriptor> > allPaths;
    _enumVisit(g, s, t, edgeCargo, maxUncovered, params, validBlockExits, validBlockEntry, blockStart, 0, path, allPaths);
    clear(path);
    if (!empty(allPaths))
        path = allPaths[0];
}

/*
template<typename TSpec, typename TVertexDescriptor, typename TEdgeCargoMap>
void
enumerateAcyclicPaths(seqan::Graph<TSpec> const& g,
                      TVertexDescriptor s,
                      TVertexDescriptor t,
                      TEdgeCargoMap const &edgeCargo,
                      UInt maxSkip,
                      seqan::StringSet<seqan::String<TVertexDescriptor> > &allPaths)
{
    // Initialization
    seqan::String<TVertexDescriptor> path;
    typedef seqan::String<std::vector<TVertexDescriptor> > TValidExitMap;
    typedef seqan::String<bool> TValidBlockEntry;

    ParamHolder params;
    params.allowedSkip = maxSkip;
    _enumVisit(g, s, t, edgeCargo, UNDEF_UINT, params, TValidExitMap(), TValidBlockEntry(), s, UNDEF_UINT, path, allPaths); //ValidExitMap is dummy
}



template<typename TSpec, typename TVertexDescriptor, typename TEdgeCargoMap>
void
enumerateAcyclicPaths(seqan::Graph<TSpec> const& g,
                      TVertexDescriptor s,
                      TVertexDescriptor t,
                      const TEdgeCargoMap &edgeCargo,
                      UInt maxUncovered,
                      UInt maxSkip,
                      seqan::StringSet<seqan::String<TVertexDescriptor> > &allPaths)
{
    // Initialization
    seqan::String<TVertexDescriptor> path;
    typedef seqan::String<std::vector<TVertexDescriptor> > TValidExitMap;
    typedef seqan::String<bool> TValidBlockEntry;

    ParamHolder params;
    params.allowedSkip = maxSkip;
    _enumVisit(g, s, t, edgeCargo, maxUncovered, params, TValidExitMap(), TValidBlockEntry(), s, UNDEF_UINT, path, allPaths); //ValidExitMap is dummy
}



template<typename TSpec, typename TVertexDescriptor, typename TEdgeCargoMap>
void
enumerateAcyclicPaths(seqan::Graph<TSpec> const& g,
                      TVertexDescriptor s,
                      TVertexDescriptor t,
                      TEdgeCargoMap const &edgeCargo,
                      seqan::String<std::vector<TVertexDescriptor> > const &validBlockExits,
                      UInt maxSkip,
                      seqan::StringSet<seqan::String<TVertexDescriptor> > &allPaths)
{
    // Initialization
    seqan::String<TVertexDescriptor> path;
    typedef seqan::String<bool> TValidBlockEntry;

    ParamHolder params;
    params.allowedSkip = maxSkip;
    params.exonAware = true;
    _enumVisit(g, s, t, edgeCargo, UNDEF_UINT, params, validBlockExits, TValidBlockEntry(), s, UNDEF_UINT, path, allPaths);
}

template<typename TSpec, typename TVertexDescriptor, typename TEdgeCargoMap>
void
enumerateAcyclicPaths(seqan::Graph<TSpec> const& g,
                      TVertexDescriptor s,
                      TVertexDescriptor t,
                      const TEdgeCargoMap &edgeCargo,
                      UInt maxUncovered,
                      seqan::String<std::vector<TVertexDescriptor> > const &validBlockExits,
                      UInt maxSkip,
                      seqan::StringSet<seqan::String<TVertexDescriptor> > &allPaths)
{
    // Initialization
    seqan::String<TVertexDescriptor> path;
    typedef seqan::String<bool> TValidBlockEntry;

    ParamHolder params;
    params.allowedSkip = maxSkip;
    params.exonAware = true;
    _enumVisit(g, s, t, edgeCargo, maxUncovered, params, validBlockExits, TValidBlockEntry(), s, UNDEF_UINT, path, allPaths);
}
*/



/**
 *  @}
 */



#endif // FUNCTIONSGRAPH_H
