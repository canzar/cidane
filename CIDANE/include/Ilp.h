/* This program is free software: you can redistribute it and/or modify
 *
 * Copyright (C) 2013-2015 Stefan Canzar, Sandro Andreotti
 *
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ILP_H_INCLUDED
#define ILP_H_INCLUDED

#include <Structures.h>
#include <map>

#include <seqan/graph_align.h>

#include <ilcplex/ilocplex.h>


float buildILP(const std::vector<Transcript> & transcripts,
               std::map<UInt, double> & computed_intens,
               const std::vector<SegmentCover> & all_covers,
               const std::vector<UInt> & selected_cover_ids,
               const std::vector<SegmentCover> & fake_covers,
               const TAllSegmentsMap & segments,
               bool de_novo);

int doGlmnet(const std::vector<Transcript> & transcripts,
             const std::vector<UInt> & locus,
             std::map<UInt, double> & computed_intens,
             const std::vector<SegmentCover> & all_covers,
             const std::vector<UInt> & selected_cover_ids,
             std::vector<bool> & cov_expl,
             const std::vector<SegmentCover> & fake_covers,
             const TAllSegmentsMap & segments,
             float & lambda,
             float & total_error,
             const std::vector<Transcript> & known_transcripts = std::vector<Transcript>());

void colgen_pe(std::vector<Transcript> & transcripts,
               const std::vector<UInt> & locus,
               std::map<UInt, double> & computed_intens,
               const std::vector<SegmentCover> & covers_in,
               const TAllSegmentsMap & segments,
               UInt read_length,
               UInt insert_size,
               std::vector<Transcript> & init_sol);


int colgen_pe_pwlin(std::vector<Transcript> & transcripts,
                    std::vector<Transcript> & new_transcripts,
                    const std::vector<UInt> & locus,
                    std::map<UInt, double> & computed_intens,
                    const std::vector<SegmentCover> & all_covers,
                    const std::vector<UInt> & selected_cover_ids,
                    std::vector<bool> & cov_expl,
                    const std::vector<SegmentCover> & fake_covers,
                    const TAllSegmentsMap & segments,
                    std::vector<Transcript> & init_sol,
                    bool tss_tes,
                    SpliceGraph & sg,
                    const std::vector<pwQuadCoeff> & coeff_bp,
                    const float lp_error,
                    UInt maxPaths,
                    const std::map<UInt, std::set<UInt> > & legalBlockBounds = std::map<UInt, std::set<UInt> >());

typedef seqan::IntervalAndCargo<UInt, UInt> TInterval;
typedef seqan::IntervalTree<UInt, UInt> TIntervalTree;

void get_locus_structure(const std::vector<UInt> & locus,
                         const TAllSegmentsMap & segments,
                         const std::map<UInt, std::set<UInt> > & legalBlockBounds,
                         std::vector<UInt> & tss,
                         std::vector<UInt> & tes,
                         std::vector<std::vector<std::pair<UInt, UInt> > > & exon_cluster,
                         std::map<UInt, UInt> & locid2exidx,
                         std::vector<std::pair<UInt, UInt> > & intron_retentions);
//std::vector<std::vector<std::pair<UInt,UInt> > >& intret_exon_cluster,
//std::map<UInt, UInt>& locid2intretexidx);

void prepareGreedyPricing(const std::vector<UInt> & locus,
                          const std::vector<SegmentCover> & selected_covers,
                          const std::map<UInt, std::set<UInt> > & legalBlockBounds,
                          seqan::String<TInterval> & mate_intervals,                        //from here result parameters
                          seqan::String<TInterval> & insert_intervals,
                          std::vector<std::vector<std::vector<std::pair<UInt, UInt> > > > & lmate,
                          std::vector<std::vector<std::vector<std::pair<UInt, UInt> > > > & rmate);

bool is_mate_compatible(const std::vector<UInt> & mate,
                        UInt left_boundary,
                        UInt right_boundary,
                        UInt current_cluster,
                        const std::map<UInt, UInt> & locid2exidx,
                        const std::vector<UInt> & locus);


#endif // ILP_H_INCLUDED
