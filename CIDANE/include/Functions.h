/* This program is free software: you can redistribute it and/or modify
 *
 * Copyright (C) 2013-2015 Stefan Canzar, Sandro Andreotti
 *
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <Structures.h>
#include <map>
#include <iostream>
#include <cmath>
#include <map>
#include <algorithm>
#include <unordered_map>
#include <Parameters.h>
#include <seqan/store.h>


//STUFF FOR HASHING SEGMENT COVERS
namespace std
{
template<>
struct hash<vector<UInt> >
{
    size_t operator()(const vector<UInt> & key) const
    {
        size_t hashVal = 0;
        for (size_t i = 0; i < key.size(); ++i)
            hashVal += std::hash<UInt>()(key[i]);
        return hashVal;
    }
};
}    
struct hashSegCov
{
    size_t operator()(const SegmentCover & key) const
    {
        size_t hashVal = 0;
        for (size_t i = 0; i < key.exons_1.size(); ++i)
            hashVal += std::hash<UInt>()(key.exons_1[i]);
        for (size_t i = 0; i < key.exons_2.size(); ++i)
            hashVal += std::hash<UInt>()(key.exons_2[i]);

        return hashVal;
    }
};

struct SegCovEq
{
    bool operator()(const SegmentCover & key1, const SegmentCover & key2) const
    {
        return (key1.exons_1 == key2.exons_1 && key1.exons_2 == key2.exons_2 );
    }
};


//typedef  std::unordered_map<std::string, int> TAbundCoverMap;
//typedef  std::unordered_map<UInt, set<UInt> > TCOverSegmentMap;
typedef  std::map<std::string, int> TAbundCoverMap;
typedef  std::map<UInt, std::set<UInt> > TCoverSegmentMap;

template <typename T1, typename T2>
double rSquared(const T1 & observed, const T2 & pred)
{
    if (observed.size() != pred.size())
    {
        std::cerr << "calling rSquared with unequal length fields -- terminating" << std::endl;
        exit(1);
    }

    double ssTot = 0;
    double ssRes = 0;
    double obsMean = 0;

    size_t len = observed.size();

    typename T1::const_iterator itObs;
    typename T2::const_iterator itPred;
    for (itObs = observed.begin(), itPred = pred.begin(); itObs != observed.end(); ++itObs, ++itPred)
    {
        obsMean += *itObs;
        ssRes += (*itObs - *itPred) * (*itObs - *itPred);
    }
    obsMean /= len;

    for (itObs = observed.begin(); itObs != observed.end(); ++itObs)
    {
        ssTot += (*itObs - obsMean) * (*itObs - obsMean);
    }

    if (ssTot < 0.000001)
        return 0;

    return 1. - (ssRes / ssTot);
}

/*!
 * \brief  Normalization function
 *
 *         This function computes the number of possible start positions in a transcript for a read (or mate pair reads) to fall into the given cover area
 *
 *
 * \param	node_cover    The cover for which the number of starting positions for a read are computed.
 * \param	node_cover    The corresponding transcript.
 * \param node_lengths  The lengths for each node id.
 * \param read_length   The read length.
 * \param insert_size   The insert size for mate pair reads.
 * \param single_paired_reads   Flag: If true and a single end cover is passed, then we sum up the number of positions for left and right ends of a mate
 *
 * \return The number of possible starting positions for a read or mate-pair in a given cover.
 */
float getNormFactor(const SegmentCover & node_cover,
                    const Transcript & transcript,
                    const TAllSegmentsMap & segments,
                    int read_length = Parameters::read_length_global,
                    int insert_size = Parameters::insert_size_global,
                    double s_dev = Parameters::s_dev,
                    bool single_paired_reads = false,
                    double cutoff = Parameters::normCutoff
                    );

/*!
 * \brief  check transcript support of segment cover
 *
 *         Check whether a given segment cover is supported by at least one transcript.
 *
 * \param cover                  The segment cover.
 * \param	transcripts            The set of transcripts.
 * \return boolean value, true if supported false otherwise
 */
bool isCoverSupported(const SegmentCover & cover, const std::vector<Transcript> & transcripts);

///*!


void generateFakeCoversSA(std::vector<SegmentCover> & fakeCovers,
                        const std::vector<SegmentCover> & allCovers,
                        const std::vector<UInt> & coverIds,
                        const std::vector<Transcript> & transcripts,
                        const TAllSegmentsMap & allSegments,
                        const std::pair<UInt, UInt> & fragLenBounds);



void generateFakeCovers(const std::vector<UInt> & locus,
                        const std::vector<SegmentCover> & all_covers,
                        const std::vector<UInt> & cover_ids,
                        const TAllSegmentsMap & all_segments,
                        std::vector<SegmentCover> & fake_covers,
                        const SpliceGraph & sg,
                        Parameters::FakeMode mode,
                        const std::pair<UInt, UInt> & fragLenBounds,
                        UInt end_space = 1,
                        UInt maxUncovered = UNDEF_UINT,
                        UInt maxExonSkip = UNDEF_UINT,
                        const SpliceGraph::TLegalExonBoundMap & legalBlockBounds = SpliceGraph::TLegalExonBoundMap()); //if nonEmpty is passed function is exonAware


void computeSingleRange(const std::vector<UInt> & nodes_cover,
                        const Transcript & transcript,
                        const TAllSegmentsMap & segments,
                        int read_length,
                        std::pair<int, int> & range);

bool isTranscriptSupported(const std::vector<SegmentCover> & all_covers,
                           const std::vector<UInt> & selected_cover_ids,
                           const Transcript & transcript,
                           const TAllSegmentsMap & allSegments,
                           const std::vector<UInt> & locus);

void findAssociatedCoversIds(std::set<UInt> & cover_ids, const std::vector<SegmentCover> & all_covers, const std::map<UInt, std::set<UInt> > & index, const std::vector<UInt> & node_ids);

std::ostream & operator<<(std::ostream & out, const SegmentCover & n);

void postproc_inf_trans(std::vector<Transcript> & input_trans,
                        std::map<UInt, double> & transcript_intens,
                        std::vector<Transcript> & output_trans,
                        std::vector<bool> & is_mandatory,
                        float thresh);

bool enumGapExonSets(const SegmentCover &, const TAllSegmentsMap &, const std::vector<UInt> &, std::vector<std::vector<bool> > &,
                     const std::map<UInt, std::set<UInt> > & legalBlockBounds = std::map<UInt, std::set<UInt> >(), int read_length = Parameters::read_length_global,
                     int fragment_length = Parameters::insert_size_global, double s_dev = Parameters::s_dev);

void addRecursiveGapExons(int, int, std::vector<std::pair<UInt, UInt> >&, std::vector<SegmentType>&, SegmentType, SegmentType, UInt, UInt, std::vector<bool>&, std::set<std::vector<bool> >&,
                          std::vector<UInt>::const_iterator low, const std::map<UInt, std::set<UInt> > & legalBlockBounds = std::map<UInt, std::set<UInt> >(), const std::set<UInt> & legal_ends = std::set<UInt>());

void stringToCover(SegmentCover & cover, const std::string & cover_string, double count, UInt uniqueCounts);

void hasUnsupportedJunction(const std::vector<UInt> & locus,
                            const std::vector<SegmentCover> & allCovers,
                            const std::vector<UInt> & selectedCoverIds,
                            const TAllSegmentsMap & allSegments,
                            const std::vector<Transcript> & transcripts,
                            std::vector<UInt> & numUnsuppJunc,
                            UInt minGapLen,
                            bool inferNeighborEdges = false,
                            bool joinOnlyCoveredSubs = true);

//std::pair<int, int> computeSingleRange(const std::vector<UInt> & nodes_cover, const Transcript &transcript, const std::map<UInt, Segment> &segments, int read_length = Parameters::read_length_global, int insert_size = Parameters::insert_size_global);

//get for a candidate transcript the number of readcounts that can possibly be explained by that transcript respecting fragment length boundaries
void computeRefinedCounts(const SegmentCover & node_cover,
                          const std::vector<Transcript> & transcripts,
                          const TAllSegmentsMap & segments,
                          const std::pair<double, double> & fragLenBounds,
                          std::vector<std::pair<std::vector<UInt>, double> > & clusters,
                          int read_length);

void markMandatoryCandidates(const std::vector<Transcript> & infTranscripts,
                             const TAllSegmentsMap & allSegments,
                             std::vector<bool> & isMandatory);

#endif /*FUNCTIONS_H*/
