/* This program is free software: you can redistribute it and/or modify
 *
 * Copyright (C) 2013-2015 Stefan Canzar, Sandro Andreotti
 *
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRUCTURES_H
#define STRUCTURES_H

#include <vector>
#include <list>
#include <set>
#include <limits.h>
#include <cstring>
#include <string>
#include <seqan/graph_types.h>
#include <stdexcept>

//forward declarations
struct Segment;
struct Transcript;

//TYPEDEFS
typedef unsigned UInt;
typedef size_t Size;

const UInt UNDEF_UINT = -1;
const size_t UNDEF_SIZE = -1;

typedef std::vector<Segment> TAllSegmentsMap;
typedef std::vector<std::vector<Transcript> > TKnownTranscriptsMap;
typedef std::vector<std::vector<UInt> > TAllLociMap;
typedef std::vector<std::set<UInt> > TKnownSpliceJunctionsMap;





/** @defgroup Structures Datastructures used in several functions
 *  @{
 */

/*!
 * \brief  SpliceJuncsMode
 *
 *         Possible modes of introducing known splice junctions into inference process
 */
enum SpliceJuncsMode
{
    SJ_NONE, //do not incorporate known splice junctions
    SJ_ADDITIONAL, //known splice junctions are added to the inferred splice junctions
    SJ_EXCLUSIVE, //use only the set of known splice junctions in SG contruction
    SJ_EXACT //use exactly the set known splice junctions
};

/*!
 * \brief  SegmentType
 *
 *         Possible Types of a segment (subexon) full exon, left end of full exon, right end of full exon, inner segment of full exon or unspecified
 */
enum SegmentType
{
    LEFTEND,
    RIGHTEND,
    INNER,
    FULL,
    UNSPECIFIED
};

/*!
 * \brief  Segment
 *
 *         Represents a Segment with some length and an id (further characteristics will probably be added)
 */
struct Segment
{
    Segment() :
        id(UNDEF_SIZE), length(0), beginPos(0), endPos(0),
        type(UNSPECIFIED), parentSegmentId(UNDEF_UINT), parentSegmentType(UNSPECIFIED), tss(true), tes(true), coverage(0), contigId(UNDEF_UINT){}
    ///The identifier of the segment
    size_t id;
    ///The length of the segment in basepairs
    UInt length;
    ///begin, end pos
    UInt beginPos, endPos;
    ///Type of subexon (LEFTEND, RIGHTEND, INNER, FULL)
    SegmentType type;    
    ///id of the parent segment (consecutive segment of adjacent subexons)
    UInt parentSegmentId;
    ///type denotes the location in parent segment (Left, Right, Full or Inner)
    SegmentType parentSegmentType;
    ///if true a possible tss
    bool tss;
    ///if true a possible tes
    bool tes;
    ///read counts involving this segment
    double coverage;
    ///contigID
    UInt contigId;

    bool operator<(const Segment & rhs)
    {
        if (contigId != rhs.contigId)
        {
            std::cerr << "Warning comparing segment on different chromosomes" << std::endl;
            throw std::runtime_error("comparing segment on different chromosomes -- exception");
        }
        if (beginPos < endPos) //forward strand
        {
            if (rhs.beginPos > rhs.endPos)
            {
                std::cerr << "Warning comparing segment on different strands" << std::endl;
                throw std::runtime_error("comparing segment on different strands -- exception");
            }
            else
            {
                return endPos <= rhs.beginPos;
            }
        }
        else //reverse strand
        {
            if (rhs.beginPos < rhs.endPos)
            {
                std::cerr << "Warning comparing segment on different strands" << std::endl;
                throw std::runtime_error("comparing segment on different strands -- exception");
            }
            else
            {
                return endPos >= rhs.beginPos;
            }
        }
    }

};


/*!
 * \brief  Segment Cover
 *
 *         Represents a segment cover which is defined by one set of spanned exons (single end reads) or two sets of spanned exons (paired end reads)
 *         together with some intensity values representing the normalized number of reads representign this cover.
 */
struct SegmentCover
{
    struct MappedRead
    {
        UInt id;
        UInt start_l;
        UInt start_r;
        double count;
    };

    typedef std::vector<UInt> TContainerType;
    typedef std::map<int, double> TDistMap;
    ///Segments covered by left mate
    TContainerType exons_1;
    ///Segments covered by right mate (empty for single end reads)
    TContainerType exons_2;
    ///represents the number of reads
    double intensity;
    ///represents the number of uniquely mapped reads    
    UInt uniqueCount;

    std::vector<MappedRead> mapped_reads;

    //for every paired end read the key is the distance between the mates of nothing is
    //in between the last subexon of the left mate and the first subexon of the right mate.
    //The associated value is the number of paired end reads having this distance
    TDistMap distCounts;

    SegmentCover() :
        intensity(0){}

    friend bool operator<(const SegmentCover & lhs, const SegmentCover & rhs);
};

//@TODO This needs to be refactored later
struct SuperSegmentCover
{
    typedef std::vector<Size> TContainerType;
    ///Segment Covers contained in set
    TContainerType covers;
    ///the intensity for the super cover
    double intensity;
};


inline bool operator<(const SegmentCover & lhs, const SegmentCover & rhs)
{
    if (lhs.exons_1.size() != rhs.exons_1.size())
        return lhs.exons_1.size() < rhs.exons_1.size();

    if (lhs.exons_2.size() != rhs.exons_2.size())
        return lhs.exons_2.size() < rhs.exons_2.size();

    UInt size = lhs.exons_1.size();
    for (UInt i = 0; i < size; ++i)
    {
        if (lhs.exons_1[i] != rhs.exons_1[i])
        {
            return lhs.exons_1[i] < rhs.exons_1[i];
        }
    }
    size = lhs.exons_2.size();
    for (UInt i = 0; i < size; ++i)
    {
        if (lhs.exons_2[i] !=  rhs.exons_2[i])
        {
            return lhs.exons_2[i] < rhs.exons_2[i];
        }
    }
    return false;
}

/*!
 * \brief  Transcript
 *
 *         Represents a transcript which is defined by an ordered set of segments and also has an id
 */
struct Transcript
{
    Transcript() :
        id(UNDEF_UINT),
        predictedExpression(-1.),
        knownExpression(-1.),
        uncovered(0),
        regWeight(1),
        isKnown(false){}

    typedef std::vector<UInt> TNodeListType;
    ///identifier
    UInt id;

    ///name
    std::string name;

    ///the ordered set of segments
    TNodeListType nodes;

    //the intensity
    double predictedExpression;

    //possibly knownExpression
    double knownExpression;

    //number of uncovered junctions
    UInt uncovered;

    //weight for regularization paramert
    double regWeight;

    //whether this is a known (annotated) transcript
    bool isKnown;

    size_t positionToSegmentIndex (size_t pos, const TAllSegmentsMap & allSegments) const
    {
        size_t nextSegBeg = 0;
        for (size_t i = 0; i < nodes.size(); ++i)
        {
            nextSegBeg += allSegments[nodes[i]].length;

            if (pos < nextSegBeg)
            {
               return i;
            }
        }
        return UNDEF_SIZE;
    }

    size_t positionToSegmentId (size_t pos, const TAllSegmentsMap & allSegments) const
    {
        size_t idx = positionToSegmentIndex (pos, allSegments);

        if (idx == UNDEF_SIZE)
        {
            return UNDEF_SIZE;
        }
        else
        {
            return nodes[idx];
        }
    }
};

inline bool operator<(const Transcript & lhs, const Transcript & rhs)
{
    if (lhs.nodes.size() != rhs.nodes.size())
        return lhs.nodes.size() < rhs.nodes.size();

    UInt size = lhs.nodes.size();
    for (UInt i = 0; i < size; ++i)
    {
        if (lhs.nodes[i] != rhs.nodes[i])
        {
            return lhs.nodes[i] < rhs.nodes[i];
        }
    }
    return false;
}

inline bool operator==(const Transcript & lhs, const Transcript & rhs)
{
    return !(lhs < rhs || rhs < lhs);
}


/*!
 * \brief  Locus
 *
 *         Represents a Locus containing the segments and possibly annotated transcripts)
 */
struct Locus
{
    ///The identifiers of the segments
    std::vector<UInt> segments;
    ///The identifiers of the transcripts
    std::vector<UInt> transcripts;
};

/**
 *  @}
 */

struct SpliceGraph
{

    friend void testSpliceGraph();
public:
    typedef seqan::Graph<seqan::Directed<> > TGraph;
    typedef seqan::VertexDescriptor<TGraph>::Type TVertexDescriptor;
    typedef seqan::EdgeDescriptor<TGraph>::Type TEdgeDescriptor;
    typedef seqan::Iterator<TGraph, seqan::EdgeIterator>::Type TEdgeIterator;
    typedef seqan::StringSet<seqan::String<TVertexDescriptor>, seqan::Owner<> > TStringSet;

    typedef std::map<UInt, TVertexDescriptor> TNodeIdMap;
    typedef std::map<TVertexDescriptor, UInt> TNodeInvIdMap;
    typedef std::map<UInt, std::set<UInt> > TLegalExonBoundMap;
    typedef std::vector<std::vector<bool> > TSuppPairMap;

    //nested struct stores indformation on edges that are used in the functions
    //to generate transcripts and fakeCovers
    struct EdgeCargo
    {
        EdgeCargo(int cov) :
            _intronEdge(true),
            _isCovered(cov),
            _edgeLen(0),
            _edgeWeight(0){}

        //flag whether edge represents a real splice junction or just a connection between adjacent subexons
        bool _intronEdge;
        //flag whether edge is covered by some read
        int _isCovered;
        //the numer of skipped exons implied by edge
        UInt _edgeLen;
        //the length of the exon on left side
        UInt _edgeWeight;
    };

    TNodeInvIdMap nodeIdsInv;
    TNodeIdMap nodeIds;

    TGraph graph, graphCov;

    TVertexDescriptor source, target;

    SpliceGraph(const std::vector<UInt> & locus,
                const TAllSegmentsMap & allSegments,                
                const TLegalExonBoundMap & legalBlockBounds = TLegalExonBoundMap());


    SpliceGraph(const std::vector<UInt> & locus,
                const std::vector<SegmentCover> & allCovers,
                const std::vector<UInt> & selectedCoverIds,
                const TAllSegmentsMap & allSegments,
                const TKnownSpliceJunctionsMap & spliceJuncs,
                const SpliceJuncsMode spliceJuncsMode,
                bool inferNeighborEdges = false,
                bool joinOnlyCoveredSubs = true,
                const TLegalExonBoundMap & legalBlockBounds = TLegalExonBoundMap());

    void getCoveredGraph(TGraph & g) const;

    void appendValidTranscripts(std::vector<Transcript> & transcripts,
                                UInt maxUncovered = UNDEF_UINT,
                                UInt maxExonSkip = UNDEF_UINT,
                                UInt maxTranscripts = UNDEF_UINT,
                                bool applyFilter = false,
                                bool exonAware = false) const;


    //check whether the gap between the two endposts can be filled with exons in between
    //s.t. their total length is within the specified min-max interval
//  bool isCoverGapSupported(const SegmentCover &cov,
//                           const std::pair<UInt, UInt> &minmaxInsert,
//                           const std::map<UInt, Segment> &allSegments,
//                           UInt maxUncovered = -1u,
//                           UInt maxExonSkip = -1u,
//                           bool exonAware = false);

    bool isCoverGapSupported(const TAllSegmentsMap & allSegments,
                             const SegmentCover & cover,
                             const std::pair<UInt, UInt> & fragLenBounds,
                             UInt maxUncovered = UNDEF_UINT,
                             UInt maxExonSkip = UNDEF_UINT,
                             bool exonAware = false) const;

    void getExonPaths(const TAllSegmentsMap & all_segments,
                      const SegmentCover & cover,
                      const std::pair<UInt, UInt> & fragLenBounds,
                      UInt maxUncovered,
                      UInt maxSkip,
                      UInt maxNumTranscripts,
                      bool exonAware,
                      std::vector<std::vector<UInt> > & exonPaths) const;

    void _getExonPaths(const TAllSegmentsMap & all_segments,
                       const SegmentCover & cover,
                       const std::pair<int, int> & fragLenBounds,
                       UInt maxUncovered,
                       UInt maxSkip,
                       UInt maxTranscripts,
                       bool exonAware,
                       std::vector<std::vector<UInt> > & exonPaths,
                       bool stopAfterFirstPath = false) const;




    TEdgeDescriptor findEdge(size_t i, size_t j, bool ignoreCovered = false) const
    {
        TVertexDescriptor vi = getNodeId(i);
        TVertexDescriptor vj = getNodeId(j);

        if (_coverBased && !ignoreCovered)
            return seqan::findEdge(graphCov, vi, vj);
        else
            return seqan::findEdge(graph, vi, vj);
    }

    bool isSegmentCovered(UInt segmentId) const
    {
        return _observedNodeIds.count(segmentId);
    }

    bool isExonAware(const std::vector<UInt> & subexonChain) const;

    inline TVertexDescriptor getNodeId(size_t segmentId) const
    {
        TNodeIdMap::const_iterator it = nodeIds.find(segmentId);
        if (it ==  nodeIds.end())
        {
            std::cerr << "invalid segment id " << segmentId << std::endl;
            throw std::invalid_argument("asked for invalid node id");
        }
        return it->second;
    }

protected:

    inline TVertexDescriptor getSegmentId(TVertexDescriptor nodeId) const
    {
        TNodeInvIdMap::const_iterator it = nodeIdsInv.find(nodeId);
        if (it ==  nodeIdsInv.end())
        {
            std::cerr << "invalid node idd " << nodeId << std::endl;
            exit(1);
        }
        return it->second;
    }

    void _setLegalBlockBounds(const std::vector<UInt> & locus,
                              const TAllSegmentsMap & allSegments,
                              const TLegalExonBoundMap & legalBlockBounds);

    void _buildGraph(const std::vector<UInt> & locus,
                     const TAllSegmentsMap & allSegments);

    bool _hasTransSupport(const seqan::String<TVertexDescriptor> & trans) const;

    void _setEdgeLengths(const TAllSegmentsMap & allSegments);


    //stores for pairs of subexons if they appear together in the two mates of a pair
    TSuppPairMap suppPairs;

    //stores for every subexon s which subexons are valid donor sites when exon started at s
    //if a subexon is a real exon it contains itself as valid exit
    seqan::String<std::vector<TVertexDescriptor> > _validBlockExit;
    //possible entries if subexon is used as exit
    seqan::String<std::vector<TVertexDescriptor> > _validBlockEntry;

    seqan::String<EdgeCargo> edgeCargo, edgeCargoCov;

    //contains all subexons that were really observed in the segment counts
    std::set<UInt> _observedNodeIds;

    //flags determining the initialization that was done
    bool _exonAware;
    bool _coverBased;
};


struct pwLinApprox
{
    double d;
    double delta;
    int nof_bp_in; //bp from 0 to d
    int nof_bp_out; //add bp to each side, 2d, 3d, ...

    static const int c = 2; //increases fine approximated inner part to scale_inner*d

    void set_d(double d);

    double f(size_t i);

    double df(size_t i);

    double bp(size_t i);

    int getNofBp();
};


struct pwQuadCoeff
{
    double bp;
    double left_lin, left_quad;
    double right_lin, right_quad;

    pwQuadCoeff(double bp, double left_lin, double left_quad, double right_lin, double right_quad);

    bool operator<(pwQuadCoeff const & rhs) const; //for sorting by bps
    bool operator<(double rhs) const; //for binary search with respect to bp values
    //bool operator=(double rhs) const;
};



struct pwQuadApprox
{
    pwQuadApprox(const std::vector<pwQuadCoeff> & coeff_bp);
    void set_d(double d);

    double d;
    const std::vector<pwQuadCoeff> & coeff_bp;

    static const int c = 1; //increases fine approximated inner part to scale_inner*d

    double l_lin_coeff, l_quad_coeff;
    double r_lin_coeff, r_quad_coeff;

    int nof_bp_in; //number of bps from -d to d
    int nof_bp_out; //additional bps 2d, 3d, ...
    double delta;

    double f(size_t i);
    double df(size_t i);
    double bp(size_t i);

    int getNofBp();
};

struct interval
{
    UInt start_coord;
    std::set<UInt>::const_iterator exitIt;
    int closed_within;
    interval(UInt start_coord, std::set<UInt>::const_iterator exitIt, int closed_within) :
        start_coord(start_coord)
        , exitIt(exitIt)
        , closed_within(closed_within)
    {}
};



#endif /*STRUCTURES_H*/
