/* This program is free software: you can redistribute it and/or modify
 *
 * Copyright (C) 2013-2015 Stefan Canzar, Sandro Andreotti
 *
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PRICEILP_H
#define PRICEILP_H

#include <Structures.h>

// ILOG stuff
#include <ilcplex/ilocplex.h>

using std::vector;
using std::map;

class PriceIlp
{
public:
    struct Options
    {
        bool _exonAware;
        bool _tsstes;
        UInt _maxUncovered;
        UInt _maxSkip;
        UInt _maxPaths;
        bool _sgComp;
        bool _ignoreIntronRet;
        bool _explainNewCover;

        //default constructor
        Options() :
            _exonAware(true)
            , _tsstes(true)
            , _maxUncovered(UNDEF_UINT)
            , _maxSkip(UNDEF_UINT)
            , _maxPaths(UNDEF_UINT)
            , _sgComp(true)
            , _ignoreIntronRet(false)
            , _explainNewCover(true)
        {}

    };

    struct Data
    {
        const vector<UInt> & locus;
        const vector<SegmentCover> & covers;
        const vector<bool> & cov_expl;
        const SpliceGraph & sg;
        const TAllSegmentsMap & segments;
        const std::map<UInt, std::set<UInt> > & legalBlockBounds;
        //const vector<vector<UInt> >& exon_cluster;
        //const map<UInt, UInt>& locid2exidx;
        const vector<std::pair<UInt, UInt> > & intron_retention;
        const vector<vector<std::pair<UInt, UInt> > > & exon_cluster;
        const map<UInt, UInt> & locid2exidx;


        Data(const vector<UInt> & locus, const vector<SegmentCover> & covers, const vector<bool> & cov_expl, const SpliceGraph & sg,
             const TAllSegmentsMap & segments, const std::map<UInt, std::set<UInt> > & legalBlockBounds,
             //const vector<vector<UInt> >& exon_cluster, const map<UInt, UInt>& locid2exidx,
             const vector<std::pair<UInt, UInt> > & intron_retention,
             const vector<vector<std::pair<UInt, UInt> > > & exon_cluster, const map<UInt, UInt> & locid2exidx) :
            locus(locus)
            , covers(covers)
            , cov_expl(cov_expl)
            , sg(sg)
            , segments(segments)
            , legalBlockBounds(legalBlockBounds)
            //, exon_cluster(exon_cluster)
            //, locid2exidx(locid2exidx)
            , intron_retention(intron_retention)
            , exon_cluster(exon_cluster)
            , locid2exidx(locid2exidx)
        {}
    };

private:
    typedef IloArray<IloBoolVarArray> TBoolVarMatrix;

    const Data & _ilpData;
    const Options & _options;   
    map<int, int> _exon_order;
    IloEnv _env;
    IloModel _model;
    IloBoolVarArray _x;
    TBoolVarMatrix _y;  //for every cover introduce set of y variables
    vector<vector<vector<bool> > > _y2exons;
    vector<bool> _cover_valid; //not sure whether this is needed

    //exon awareness
    IloBoolVarArray _es;
    IloBoolVarArray _ee;

    //tss/tes
    IloBoolVarArray _s;
    IloBoolVarArray _e;
    IloBoolVarArray _pre;
    IloBoolVarArray _suff; //attention: suff[i] refers to variable x[i+1]

    //intron retentions
    IloBoolVarArray _ir;

    vector<IloBoolVar> _rx_sgc; //indicates relaxation of sg compatibility - new edge
    vector<IloBoolVar> _knownExonVars; //variables for known exons required for exon awareness

    IloObjective _obj;
    IloExtractable _force_expl_constr;
    map<std::pair<UInt, UInt>, UInt> _relaxed_edges;   //rhs var of constraint relaxing sg compatibility

    IloCplex _cplex;

    void initXY();   
    void initExonAwareNEW();   
    void initTssTesNEW();
    void initObjective();
    void initCplex();
    void initSgComp();
    void initIntronRet();

public:
    PriceIlp(const Data & ilpData, const Options & options);
    ~PriceIlp();
    bool init();
    void updateObjDual(const IloNumArray & duals);
    bool solve();
    double getObjValue() const;
    void printCplexStatus() const;
    void getNewTrans(vector<bool> & new_trans, vector<double> & ll);
    const vector<bool> & getCoverValid() const;
    void printX();
    bool explainNewCover();
    bool updateNewCover();
    bool updateSgComp();
    bool is_infeasible() const;
    bool hasRelaxedEdges() const
    {
        return _relaxed_edges.size() != 0;
    }

};

#endif
