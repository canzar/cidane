/* This program is free software: you can redistribute it and/or modify
 *
 * Copyright (C) 2013-2015 Stefan Canzar, Sandro Andreotti
 *
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FUNCTIONSPREPROC_H
#define FUNCTIONSPREPROC_H

#include <vector>

#include <Structures.h>
#include <Functions.h>


/** @defgroup Preprocess Functions to preprocess (filter, normalize) input data *
 *  @{
 */


/*!
 * \brief Tests whether a given has maximal terminals.
 * With the current setting of tss and tes sites and for exon aware the result displays whether the whole transcript is maximal or not.
 * \param spliced_l and spliced_r denote whether given segment has a real covered splice site on its right or left end.
 */
bool hasMaximalTerminals(const Transcript & transcript,
                         const std::vector<UInt>& locus,
                         const std::map<size_t, size_t>& locus_inv,
                         const std::vector<bool>& spliced_l,
                         const std::vector<bool>& spliced_r,
                         const TAllSegmentsMap & allSegments,
                         const SpliceGraph::TLegalExonBoundMap & legalBlockBounds,
                         bool debug = false);


//infer TSS/TES information from read mappings (without annotation and known transcripts)
void identifyTssTes(const std::vector<SegmentCover> & allCovers,
                    const std::vector<UInt> & selectedCoverIds,
                    TAllSegmentsMap & allSegments,
                    const std::vector<UInt> & selectedSegmentIds,
                    double threshold,
                    bool onlyCoveredTerminals);


//infer TSS/TES information from known Transcripts
void getTssTesFromAnnotation(TAllSegmentsMap & allSegments,
                             const TKnownTranscriptsMap & knownTranscripts);



void regroupLoci(const std::vector<SegmentCover> & segment_covers,
                 TAllLociMap & all_loci,
                 const TAllSegmentsMap & all_segments);





#endif // FUNCTIONSPREPROC_H
