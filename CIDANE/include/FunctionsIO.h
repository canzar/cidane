/* This program is free software: you can redistribute it and/or modify
 *
 * Copyright (C) 2013-2015 Stefan Canzar, Sandro Andreotti
 *
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FUNCTIONSIO_H
#define FUNCTIONSIO_H

#include <string>
#include <map>
#include <iostream>
#include <vector>


#include <Structures.h>
#include <seqan/store.h>


/** @defgroup IO Input-Output Functions *
 *  @{
 */


void readQuadApproxCoeff(const std::string & filename, std::vector<double> & breakpoints, std::vector<std::pair<double, double> > & left_coeff, std::vector<std::pair<double, double> > & right_coeff, std::vector<pwQuadCoeff> & coeff_bp);




/*!
 * \brief  read segment covers from file
 *
 *         Reads a .cnt file and stores entries in two maps. Every segment cover is stored in vector covers.
 *
 * \param filename               Name of the input .cnt file.
 * \param covers                 Holds the read segment covers.
 * \param	covers_to_segment      Index map to get all segment covers containing a certain segment. key: segment id, value: set of all covers containing this segment.
 */
void readSegmentCovers(std::string filename, std::vector<SegmentCover> & covers, std::map<UInt, std::set<UInt> > & covers_to_segment);

/*!
 * \brief  read segment covers from read mapping file
 *
 *         Reads a tople details file and stores entries in two maps. Every segment cover is stored in vector covers.
 *
 * \param filename               Name of the input tuple_details file.
 * \param covers                 Holds the read segment covers.
 * \param	covers_to_segment      Index map to get all segment covers containing a certain segment. key: segment id, value: set of all covers containing this segment.
 */

template <typename TtranscriptsByLocus, typename TGffInfileName, typename TGffOutfileName>
void writeAnnotationFile(const TtranscriptsByLocus & transcripts,
                         const TGffInfileName & inFile,
                         const TGffOutfileName & outFile
                         );
/**
 *  @}
 */


//----------------------------------------------------------------------------------------------------------------
//------------------------------DEFINITION OF FUNCTION TEMPLATES--------------------------------------------------
//----------------------------------------------------------------------------------------------------------------

template <typename TLociMap, typename TSegMap, typename TTransMap, typename TTransIdMap, typename TValidExitsMap>
void readAnnotationFile(const std::string & annotFile,
                        const std::string & knownExpressionsFile,
                        TLociMap & allLoci,
                        TSegMap & allSegments,
                        TTransMap & knownTranscripts,
                        TTransIdMap & transIdToName,
                        TValidExitsMap & validExits)
{

    typedef seqan::FragmentStore<> TFragmentStore;
    typedef typename TFragmentStore::TAnnotationStore TAnnotationStore;
    typedef typename seqan::Iterator<TFragmentStore, seqan::AnnotationTree<> >::Type TAnnoTreeIter;
    //typedef typename seqan::Value<TAnnoTreeIter>::Type TId;
    typedef typename seqan::Value<TAnnotationStore>::Type TAnnotation;
    typedef TAnnotation::TPos TPos;
    typedef std::map<std::string, double> TKnownExpressionsMap;

    //if given read the white list containing the transcripts that should go into the knownTranscripts
    bool useKnownExpressions = false;
    TKnownExpressionsMap knownExpressions;
    if (knownExpressionsFile != "")
    {
        useKnownExpressions = true;
        std::ifstream in_wl_file(knownExpressionsFile.c_str(), std::ios::binary);
        if (in_wl_file.is_open())
        {
            std::string line;
            std::stringstream strstr;
            while (std::getline(in_wl_file, line))
            {
                strstr.clear();
                strstr.str(line);
                std::string id;
                double fpkm = 0.;
                strstr >> id >> fpkm;
                knownExpressions[id] = fpkm;
            }
        }
        else
        {
            std::cerr << "Could not open white list file " << knownExpressionsFile << std::endl;
            exit(1);
        }
    }

    //read the annotation file
    TFragmentStore store;
    seqan::GffFileIn inFile;
    //std::ifstream inFile(annotFile.c_str(), std::ios::binary);
    //if(!inFile.is_open())
    if (!open(inFile, annotFile.c_str()))
    {
        std::cerr << "could not open file " << annotFile << std::endl;
        exit(1);
    }
    readRecords(store, inFile);
    //read(inFile, store, seqan::Gtf());
    //inFile.close();

    UInt exonType = 0;
    UInt subExonType = 0;
    UInt geneType = 0;
    UInt transcriptType = 0;

    seqan::_storeAppendType(store, exonType, "exon");
    seqan::_storeAppendType(store, subExonType, "subexon");
    seqan::_storeAppendType(store, geneType, "gene");
    seqan::_storeAppendType(store, transcriptType, "mRNA");

    //iterate through annotations and gather informations about transcripts and segments
    TAnnoTreeIter dfsIt = seqan::begin(store, seqan::AnnotationTree<>());


    std::map<size_t, std::set<size_t> > tmpLoci;

    size_t tId = 0;
    size_t locId = UNDEF_SIZE;

    size_t firstSub = UNDEF_SIZE;
    size_t lastSub = UNDEF_SIZE;
    TPos lastEnd = -1;

    seqan::CharString nodeIdStr;
    seqan::CharString transIdStr;
    seqan::CharString geneIdStr;
    bool newTrans = false;

    Transcript * activeTransPtr = NULL;


    while (!atEnd(dfsIt))
    {
        TAnnotation & anno = getAnnotation(dfsIt);
        UInt contigId = anno.contigId;

        if (anno.typeId == geneType)
        {
            geneIdStr = seqan::getName(dfsIt);
            ++locId;
        }
        if (anno.typeId == transcriptType)
        {
            transIdStr = seqan::getName(dfsIt);
            newTrans = true;
        }

        if (anno.typeId == subExonType)
        {
            seqan::annotationGetValueByKey(nodeIdStr, store, anno, "NodeId");

            //std::cerr << nodeIdStr << std::endl;
            //std::cerr << nodeIdStr << " " << transIdStr << " " << geneIdStr << std::endl;

            size_t nodeId;
            std::stringstream strstr(seqan::toCString(nodeIdStr));
            strstr >> nodeId;

            bool strand = anno.beginPos < anno.endPos;

            if (((strand && lastEnd != anno.beginPos) ||
                 (!strand && lastEnd != anno.endPos)))
            {
                if (firstSub != UNDEF_SIZE)
                {
                    if (strand)
                    {
                        validExits[firstSub].insert(lastSub);
                        assert(firstSub <= lastSub);
                    }
                    else
                    {
                        validExits[lastSub].insert(firstSub);
                        assert(firstSub >= lastSub);
                    }
                    //std::cerr << "FirstSub: " << firstSub << "  LastSub: " << lastSub << std::endl;
                }
                firstSub = nodeId;
            }
            lastSub = nodeId;

            TAnnoTreeIter tmpIt = dfsIt;
            ++tmpIt;
            bool isLastSub = atEnd(tmpIt);
            if (!atEnd(tmpIt))
            {
                isLastSub = getAnnotation(tmpIt).typeId != subExonType;
            }
            if (isLastSub)
            {
                if (strand)
                {
                    validExits[firstSub].insert(lastSub);
                    assert(firstSub <= lastSub);
                }
                else
                {
                    validExits[lastSub].insert(firstSub);
                    assert(firstSub >= lastSub);
                }
                firstSub = lastSub = UNDEF_SIZE;
                lastEnd = -1;
            }
            else
            {
                if (strand)
                    lastEnd = anno.endPos;
                else
                    lastEnd = anno.beginPos;
            }

            if (newTrans)
            {
                newTrans = false;

                Transcript t;
                t.id = tId;
                t.name = seqan::toCString(transIdStr);

                transIdToName[tId] = seqan::toCString(transIdStr);
                ++tId;

                if (useKnownExpressions)
                {
                    TKnownExpressionsMap::const_iterator it = knownExpressions.find(seqan::toCString(transIdStr));
                    if (it != knownExpressions.end())
                        t.knownExpression = it->second;
                }

                if (knownTranscripts.capacity() <= locId)
                    knownTranscripts.reserve(2 * locId);

                if (knownTranscripts.size() <= locId)
                    knownTranscripts.resize(locId + 1);

                knownTranscripts[locId].push_back(t);
                activeTransPtr = &knownTranscripts[locId].back();

            }

            if (activeTransPtr != NULL)
            {
                //fill the actual transcript
                activeTransPtr->nodes.push_back(nodeId);
            }

            Segment s;
            s.contigId = contigId;
            seqan::CharString segType;
            if (seqan::annotationGetValueByKey(segType, store, anno, "SpliceEnd"))
            {
                if (segType == "L")
                    s.type = LEFTEND;
                else if (segType == "R")
                    s.type = RIGHTEND;
                else if (segType == "-")
                    s.type = INNER;
                else if (segType == "B")
                    s.type = FULL;
            }
            else
            {
                s.type = UNSPECIFIED;
            }
            //for parsing grit generated refined.gtf with annotated tss/tes sites
            seqan::CharString exonType;
            if (seqan::annotationGetValueByKey(exonType, store, anno, "ExonType"))
            {
                s.tss = s.tes = false;

                if (exonType == "tss")
                    s.tss = true;
                else if (exonType == "tes")
                    s.tes = true;
                else if (exonType == "single")
                {
                    s.tss = true;
                    s.tes = true;
                }
            }

            s.id = nodeId;
            s.beginPos = anno.beginPos;
            s.endPos = anno.endPos;
            s.length = abs(anno.endPos - anno.beginPos);

            if (allSegments.capacity() <= nodeId)
                allSegments.reserve(2 * nodeId);
            if (allSegments.size() <= nodeId)
                allSegments.resize(nodeId + 1);

            allSegments[nodeId] = s;

            tmpLoci[locId].insert(nodeId);
        }
        ++dfsIt;
    }

    std::map<size_t, std::set<size_t> >::const_iterator locIt;
//  if (!tmpLoci.empty())
//    allLoci.resize(tmpLoci.rbegin()->first);
    allLoci.resize(tmpLoci.size());

    for (locIt = tmpLoci.begin(); locIt != tmpLoci.end(); ++locIt)
    {
        allLoci[locIt->first].assign(locIt->second.begin(), locIt->second.end());

        //sort knownTranscript subexons
        std::vector<Transcript>::iterator tit = knownTranscripts[locIt->first].begin();
        for (; tit != knownTranscripts[locIt->first].end(); ++tit)
        {
            if (tit->nodes.size() > 1 && tit->nodes[0] > tit->nodes[1])
                std::reverse(tit->nodes.begin(), tit->nodes.end());
        }
    }

    //group subexons to parent segments (consecutive region of adjacent subexons)
    //every subexon is then also marked as first, last, inner part of the segment
    UInt nextSegId = 0;
    for (typename TLociMap::iterator allLocIt = allLoci.begin(); allLocIt != allLoci.end(); ++allLocIt)
    {
        UInt lastEnd = UNDEF_UINT;
        UInt lastId = UNDEF_UINT;

        typedef typename TLociMap::value_type::iterator TLocIt;
        for (TLocIt locIt = allLocIt->begin(); locIt != allLocIt->end(); ++locIt)
        {
            if (lastEnd != allSegments[*locIt].beginPos)
            {
                ++nextSegId;
                allSegments[*locIt].parentSegmentType = LEFTEND;

                //adjust entry for last subexon of previous segment -- either full or rightend
                if (lastId != UNDEF_UINT)
                {
                    if (allSegments[lastId].parentSegmentType == LEFTEND)
                        allSegments[lastId].parentSegmentType = FULL;
                    else
                        allSegments[lastId].parentSegmentType = RIGHTEND;
                }
            }
            else
            {
                //initially every non first (non Left) is marked as inner
                allSegments[*locIt].parentSegmentType = INNER;
            }

            allSegments[*locIt].parentSegmentId = nextSegId;
            lastEnd = allSegments[*locIt].endPos;
            lastId = *locIt;
        }

        //adjust last subexon of segment
        if (allSegments[lastId].parentSegmentType == INNER)
            allSegments[lastId].parentSegmentType = RIGHTEND;
        else if (allSegments[lastId].parentSegmentType == LEFTEND)
            allSegments[lastId].parentSegmentType = FULL;
    }

}

template <typename TtranscriptsByLocus, typename TGffInfileName, typename TGffOutfileName>
void writeAnnotationFile(const TtranscriptsByLocus & transcripts,
                         const TGffInfileName & inFileName,
                         const TGffOutfileName & outFileName,
                         bool useKnownGeneIds)
{
    //std::ifstream inFile(inFileName.c_str(), std::ios::binary);
    seqan::GffFileIn inFile(inFileName.c_str());
    seqan::GffFileOut outFile(outFileName.c_str());


    //std::ofstream outFile(outFileName.c_str(), std::ios::binary);

    //load the refined gff file and collect the subexon annotations
    typedef seqan::FragmentStore<> TFragmentStore;
    typedef typename TFragmentStore::TAnnotationStore TAnnotationStore;
    typedef typename seqan::Iterator<TFragmentStore, seqan::AnnotationTree<> >::Type TAnnoTreeIter;
    typedef typename seqan::Value<TAnnoTreeIter>::Type Tid;
    typedef typename seqan::Value<TAnnotationStore>::Type TAnnotation;
    //typedef typename std::map<seqan::CharString, std::set<std::pair<int, int> > > ExonMap;
    typedef typename TtranscriptsByLocus::const_iterator TGeneIter;
    //typedef typename seqan::Value<TtranscriptsByLocus>::Type::const_iterator TTransIter;
    typedef std::vector<Transcript>::const_iterator TTransIter;
    typedef typename Transcript::TNodeListType::const_iterator TNodeIter;

    bool add_cds = false;

    UInt subExonType = 0;
    UInt geneType = 0;

    TFragmentStore store, store_out;
    seqan::_storeAppendType(store, subExonType, "subexon");
    seqan::_storeAppendType(store, geneType, "gene");
    std::cerr << "Loading GFF" << std::endl;
    readRecords(store, inFile);
    std::cerr << "Loading Done" << std::endl;

    std::map<seqan::CharString, Tid> subexons;
    std::vector<std::string> orig_names;

    TAnnoTreeIter dfsIt = seqan::begin(store, seqan::AnnotationTree<>());

    std::cerr << "Reading subexon information" << std::endl;
    while (!atEnd(dfsIt))
    {
        TAnnotation & anno = getAnnotation(dfsIt);
        if (useKnownGeneIds && (anno.typeId == geneType))
        {
            orig_names.push_back(seqan::toCString(seqan::getName(dfsIt)));
        }
        if (anno.typeId == subExonType)
        {
            seqan::CharString nodeIdStr;
            seqan::annotationGetValueByKey(nodeIdStr, store, anno, "NodeId");
            subexons[nodeIdStr] = value(dfsIt);
        }
        ++dfsIt;
    }
    std::cerr << "Reading subexon information done" << std::endl;
    std::cerr << "Generate Annotation..." << std::endl;

    store_out.contigNameStore = store.contigNameStore;
    //seqan::clear(store.annotationStore);
    TAnnoTreeIter root = seqan::begin(store_out, seqan::AnnotationTree<>());
    TGeneIter gene_it = transcripts.begin();
    UInt nextGeneId = 0;
    while (gene_it != transcripts.end())
    {
        //seqan::resize(store_out.annotationNameStore, seqan::length(store_out.annotationNameStore) + 1);
        TAnnoTreeIter gene = seqan::createRightChild(root);
        TAnnotation & anno_gene = getAnnotation(gene);
        anno_gene.typeId = store_out.ANNO_GENE;
        if (useKnownGeneIds)
        {
            setName(gene, orig_names[nextGeneId]);
        }
        else
        {
            std::stringstream strstr;
            strstr << nextGeneId;
            std::string annoNameStr = strstr.str();
            setName(gene, annoNameStr);
        }
        
        //now add the child transcripts
        TTransIter trans_it = gene_it->begin();
        UInt trans_num = 1;
        while (trans_it != gene_it->end())
        {
            //seqan::resize(store_out.annotationNameStore, seqan::length(store_out.annotationNameStore) + 1);
            TAnnoTreeIter trans = createRightChild(gene);
            //add it here to be on top of the entries for that transcript
            TAnnoTreeIter transTag = createRightChild(trans);
            //std::cerr <<" create transcript: " << std::endl;

            TAnnotation & anno_mRNA = getAnnotation(trans);
            anno_mRNA.beginPos = TAnnotation::INVALID_POS;
            anno_mRNA.endPos = TAnnotation::INVALID_POS;
            anno_mRNA.typeId = store_out.ANNO_MRNA;
            std::string t_name;
            
            if (trans_it->name != "")
            {
                t_name = trans_it->name;
            }
            else
            {
                std::stringstream strstr1;                
                strstr1 << getName(gene) << "." << trans_num++;
                //mark transcripts with uncovered junctions by CG tag.
                if (trans_it->uncovered > 0)
                {
                    strstr1 << "_CG";
                }
                t_name = strstr1.str();
            }
            setName(trans, t_name);

            double fpkm = trans_it->predictedExpression;

            std::stringstream strstr;
            strstr << fpkm;
            seqan::annotationAssignValueByKey(store_out, anno_mRNA, "FPKM", strstr.str());
           
            //get the exons with their start-end positions
            std::vector<std::pair<UInt, UInt> > exons; 
            UInt contigId = UNDEF_UINT;
            TNodeIter node_it = trans_it->nodes.begin();
            while (node_it != trans_it->nodes.end())
            {
                std::stringstream strstr;
                strstr << *node_it;
                std::string node_id = strstr.str();
                if (subexons.find(node_id) == subexons.end())
                {
                    std::cerr <<  "no entry for: " << node_id << std::endl;
                    ++node_it;
                    continue;
                }
                TAnnotation & anno_sub = store.annotationStore[subexons[node_id]];
                if (!exons.empty() && exons.back().second == anno_sub.beginPos)
                {
                    exons.back().second = anno_sub.endPos;
                }
                else 
                {
                    contigId = anno_sub.contigId;
                    exons.push_back(std::make_pair(anno_sub.beginPos, anno_sub.endPos));
                }
                ++node_it;
            }
            //use correct ordering on reverse strand
            if (exons.front().first > exons.back().second)
            {
                std::reverse(exons.begin(), exons.end());
            }

            for (size_t i = 0; i < exons.size(); ++i)
            {
                TAnnoTreeIter exon = seqan::createRightChild(trans);
                TAnnotation & anno_exon = getAnnotation(exon);
                anno_exon.typeId = store_out.ANNO_EXON;
                anno_exon.contigId = contigId;
                anno_exon.beginPos = exons[i].first;
                anno_exon.endPos = exons[i].second;

                if (add_cds)
                {
                    TAnnoTreeIter cds = seqan::createSibling(exon);
                    TAnnotation & anno_cds = getAnnotation(cds);
                    anno_cds.typeId = store_out.ANNO_CDS;
                }
                seqan::_adjustParent(getAnnotation(trans), getAnnotation(exon));
            }
/*
            tnodeiter node_it = trans_it->nodes.begin();
            tannotreeiter exon, cds;
            while (node_it != trans_it->nodes.end())
            {
                std::stringstream strstr;
                strstr << *node_it;
                std::string node_id = strstr.str();
                if (subexons.find(node_id) == subexons.end())
                {
                    std::cerr <<  "no entry for: " << node_id << std::endl;
                    ++node_it;
                    continue;
                }
                TAnnotation & anno_sub = store.annotationStore[subexons[node_id]];
                if (node_it != trans_it->nodes.begin())
                {
                    TAnnotation & anno_last_exon = getAnnotation(exon);
                    if (anno_last_exon.endPos == anno_sub.beginPos)
                    {
                        //std::cerr << "merging: [" << anno_last_exon.beginPos << "," << anno_last_exon.endPos << "]" <<
                        //"and [" << anno_sub.beginPos << "," << anno_sub.endPos << "]"<< "   t_id: " << trans_name_str <<"  " <<trans_it->nodes.size() << std::endl;
                        anno_last_exon.endPos = anno_sub.endPos;
                        if (add_cds)
                        {
                            TAnnotation & anno_last_cds = getAnnotation(cds);
                            anno_last_cds.endPos = anno_sub.endPos;
                        }
                        ++node_it;
                        seqan::_adjustParent(getAnnotation(trans), anno_last_exon);
                        continue;
                    }
                }
                exon = seqan::createRightChild(trans);
                TAnnotation & anno_exon = getAnnotation(exon);
                anno_exon.typeId = store_out.ANNO_EXON;
                anno_exon.contigId = anno_sub.contigId;
                anno_exon.beginPos = anno_sub.beginPos;
                anno_exon.endPos = anno_sub.endPos;
                ++node_it;

                if (add_cds)
                {
                    cds = seqan::createSibling(exon);
                    TAnnotation & anno_cds = getAnnotation(cds);
                    anno_cds.typeId = store_out.ANNO_CDS;
                }
                std::cout << "before: " << getname(trans) << " " << getAnnotation(trans).beginPos << " " << getAnnotation(trans).endPos << std::endl;
                seqan::_adjustParent(getAnnotation(trans), getAnnotation(exon));
                std::cout << "after: " << getAnnotation(trans).beginPos << " " << getAnnotation(trans).endPos << std::endl;
            }
*/
            seqan::getAnnotation(transTag).contigId = getAnnotation(trans).contigId;
            seqan::getAnnotation(transTag).beginPos = getAnnotation(trans).beginPos;
            seqan::getAnnotation(transTag).endPos = getAnnotation(trans).endPos;
            seqan::setType(transTag, "transcript");
            seqan::_adjustParent(getAnnotation(gene), getAnnotation(trans));
            assert(getAnnotation(gene).contigId == getAnnotation(trans).contigId);

            ++trans_it;
        }
        ++gene_it;
        ++nextGeneId;
    }
    std::cerr << "Generating done" << std::endl;

    std::cerr << "Writing outfile ..." << std::endl;
    //seqan::write(outFile, store_out, seqan::Gtf());
    seqan::writeRecords(outFile, store_out);
    std::cerr << "Writing outfile done" << std::endl;
//  inFile.close();
//  outFile.close();
}

#endif // FUNCTIONSIO_H
